package loxe

import (
	"fmt"
	"io"
)

func (e *loxerror) Format(s fmt.State, verb rune) {
	_, err := io.WriteString(s, e.msg)
	if err != nil {
		panic(err)
	}

	if s.Flag('+') && e.stack != nil {
		_, err := io.WriteString(s, "\n"+e.stack.toTerminal())
		if err != nil {
			panic(err)
		}
	}
}
