package generate

import (
	"bytes"
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/util"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/input"
	"os"
	"text/template"
)

var cliUsage = func() func() {
	tmpl, e := template.New("").Parse(cliUsageHelpTemplate)
	if e != nil {
		panic(e)
	}

	type tmplVars struct {
		OutputFileName          string
		OutputFileNameDefault   string
		OutputFolderPath        string
		OutputFolderPathDefault string
		OutputLanguage          string
		OutputLanguageDefault   string
		AvailableLanguages      string
		WithHelpers             string
		WithHelpersDefault      string

		Debug               string
		DebugDefault        string
		SupportsAnsi        string
		SupportsAnsiDefault string
		Workdir             string
		WorkdirDefault      string
	}
	vars := tmplVars{
		OutputFileName:          util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputFileName),
		OutputFileNameDefault:   util.YellowString("Default: " + input.DefaultArguments.OutputFileName),
		OutputFolderPath:        util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputFolderPath),
		OutputFolderPathDefault: util.YellowString("Default: " + input.DefaultArguments.OutputFolderPath),
		OutputLanguage:          util.CyanString("--" + input.MetaᐸArgumentsᐳ.OutputLanguage),
		OutputLanguageDefault:   util.YellowString("Default: " + string(input.DefaultArguments.OutputLanguage)),
		AvailableLanguages:      util.CyanString(string(Typescript)),
		WithHelpers:             util.CyanString("--" + input.MetaᐸArgumentsᐳ.WithHelpers),
		WithHelpersDefault:      util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.WithHelpers)),

		Debug:               util.CyanString("--" + input.MetaᐸArgumentsᐳ.Debug),
		DebugDefault:        util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.Debug)),
		SupportsAnsi:        util.CyanString("--" + input.MetaᐸArgumentsᐳ.SupportsANSI),
		SupportsAnsiDefault: util.YellowString(fmt.Sprintf("Default: %t", input.DefaultArguments.SupportsANSI)),
		Workdir:             util.CyanString("--" + input.MetaᐸArgumentsᐳ.Workdir),
		WorkdirDefault:      util.YellowString("Default: " + input.DefaultArguments.Workdir),
	}

	var msg bytes.Buffer
	e = tmpl.Execute(&msg, vars)
	if e != nil {
		panic(e)
	}

	return func() {
		fmt.Println(msg.String())
		os.Exit(0)
	}
}()

const cliUsageHelpTemplate = `This CLI is used to generate strong-typed client code synchronized with the server Loxerrors. Usage:
	Output-related flags:
		{{.OutputFileName}}: The name of the generated file
			{{.OutputFileNameDefault}}
		{{.OutputFolderPath}}: The path to the folder to save the generated file
			{{.OutputFolderPathDefault}}
		{{.OutputLanguage}}: The target programming language to use to generate the client code
			Available languages: {{.AvailableLanguages}}
			{{.OutputLanguageDefault}}
		{{.WithHelpers}}: Set this flag to true if you need helpers to handle the Loxerror dictionary
			Note that if you're generating code for many modules, you just need to set this flag once (otherwise the code will duplicate)
			{{.WithHelpersDefault}}

	Other flags:
		{{.Debug}}: This flag controls the level of runtime log information
			If true, additional runtime information will be displayed (aka DEBUG logs)
			{{.DebugDefault}}
		{{.SupportsAnsi}}: If true, the log output will contain ANSI characters
			Set it to true if you terminal supports ANSI characters
			Currently, ANSI escape codes are used to colorize strings and draw "nested logs"
			Some control sequences can only access visible terminal content. Logs with many "nested child logs" will behave unexpectedly
			If you need to debug something, it can help you
			** EXPERIMENTAL FEATURE **
			{{.SupportsAnsiDefault}}
		{{.Workdir}}: Use this flag to set the current work directory
			This will be used to resolve package import paths and other things related to the underlying build tool
			{{.WorkdirDefault}}
`
