package code

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	loxe "gitlab.com/loxe-tools/go-loxerror"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates"
)

func Identification(rootModules []loxe.LoxerrorMod, tmpls *templates.Templates, log *logCLI.LogCLI) string {
	code := ""
	for _, currModule := range rootModules {
		code += identificationRecursively(currModule, tmpls,
			log.Debug("Generating root module '%s' errors identification functions", currModule.Name())) + "\n"
	}

	log.Debug("Ok!")
	return code
}

func identificationRecursively(module loxe.LoxerrorMod, tmpls *templates.Templates, log *logCLI.LogCLI) string {
	identificationCode, e := tmpls.Identification(templates.IdentificationVars{
		module,
		templates.LoxerrorTypeName,
		loxe.ModuleJsonField,
		loxe.CodeJsonField,
	})
	if e != nil {
		log.Fatal("Error: %v", e)
	}
	if len(module.SubModules()) == 0 {
		return identificationCode
	}

	for _, currSubModule := range module.SubModules() {
		identificationCode += "\n" + identificationRecursively(currSubModule, tmpls,
			log.Debug("Generating subModule '%s' errors identification functions", currSubModule.Name()))
	}
	return identificationCode
}
