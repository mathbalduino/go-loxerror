package generate

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/tsFile"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-loxerror"
	"gitlab.com/loxe-tools/go-loxerror/internal"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/input"
)

const libVersion = internal.LibraryModulePath + " " + internal.LibraryModuleVersion

func Generate(arguments input.Arguments, log *logCLI.LogCLI, rootModules ...loxe.LoxerrorMod) {
	arguments = handleParams(arguments, log, rootModules)

	tmpls := createLangTemplates(arguments.OutputLanguage, log.Debug("Creating code templates..."))
	dictionaryCode := code.Dictionary(rootModules, tmpls, log.Debug("Starting root modules dictionary code generation..."))
	identificationCode := code.Identification(rootModules, tmpls, log.Debug("Starting root modules errors identification functions code generation..."))

	file := tsFile.NewTsFile(arguments.OutputFileName)
	file.AddCode(dictionaryCode)
	file.AddCode(identificationCode)

	if arguments.WithHelpers {
		helpersCode := code.Helpers(tmpls, log.Debug("Starting helpers code generation..."))
		file.AddCode(helpersCode)
	}

	saveFilesLog := log.Debug("Saving generated file to disk...")
	e := file.Save(libVersion, arguments.OutputFolderPath)
	if e != nil {
		saveFilesLog.Fatal("Cannot save Dictionary file to disk: %v", e)
	}
	saveFilesLog.Info("File '%s' saved to disk", arguments.OutputFileName)
	log.Info("The end!")
}
