package input

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"path/filepath"
)

func Normalize(arguments Arguments, log *logCLI.LogCLI) Arguments {
	if !filepath.IsAbs(arguments.Workdir) {
		absWorkdir, e := filepath.Abs(arguments.Workdir)
		if e != nil {
			log.Fatal("%v", e)
		}
		arguments.Workdir = absWorkdir
		log.Debug("--%s path converted to it's absolute path equivalent", MetaᐸArgumentsᐳ.Workdir)
	}
	if !filepath.IsAbs(arguments.OutputFolderPath) {
		absPath, e := filepath.Abs(fmt.Sprintf("%s/%s", arguments.Workdir, arguments.OutputFolderPath))
		if e != nil {
			log.Fatal("%v", e)
		}

		arguments.OutputFolderPath = absPath
		log.Debug("--%s path value converted to it's absolute path equivalent", MetaᐸArgumentsᐳ.OutputFolderPath)
	}

	return arguments
}
