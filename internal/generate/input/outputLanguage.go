package input

type OutputLanguage string

const (
	Typescript OutputLanguage = "typescript"
)

func (o OutputLanguage) IsValid() bool {
	switch o {
	case Typescript:
		return true
	default:
		return false
	}
}
