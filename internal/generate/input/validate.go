package input

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation"
)

func Validate(args Arguments, log *logCLI.LogCLI) {
	v := schemaArguments(args)
	if v.IsValid() {
		return
	}

	collectedError := loxe.BasicCollector(v)
	msg := "CLI arguments validation errors:"
	if collectedError.RootError != nil {
		msg += fmt.Sprintf("\n%s\n", collectedError.RootError)
	}
	if len(collectedError.Fields) != 0 {
		msg += "\nFlag specific errors:"
		for fieldName, err := range collectedError.Fields {
			msg += fmt.Sprintf("\n\t--%s: %s", fieldName, err)
		}
	}

	log.Fatal(msg)
}
