package loxe

func (e *loxerror) Wrap(err Loxerror) Loxerror {
	newErr := *e
	newErr.wrapped = err
	return &newErr
}
